<?php

namespace Drupal\workflow_task\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Workflow task entities.
 *
 * @ingroup workflow_task
 */
class WorkflowTaskDeleteForm extends ContentEntityDeleteForm {


}
