<?php

namespace Drupal\workflow_task\Plugin\WorkflowType;

use Drupal\workflows\WorkflowTypeInterface;

/**
 * Interface for TaskWorkflowType WorkflowType plugin.
 */
interface TaskWorkflowTypeInterface extends WorkflowTypeInterface {

}
